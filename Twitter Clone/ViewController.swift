//
//  ViewController.swift
//  Twitter Clone
//
//  Created by Yıldırımhan Atçıoğlu on 16.01.2018.
//  Copyright © 2018 Yıldırımhan Atçıoğlu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var signUpButton: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        signUpButton.layer.cornerRadius = 5.0 // buttona ovallik verme-corner radius
        signUpButton.layer.masksToBounds = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


//
//  MyTweetViewController.swift
//  Twitter Clone
//
//  Created by Yıldırımhan Atçıoğlu on 18.01.2018.
//  Copyright © 2018 Yıldırımhan Atçıoğlu. All rights reserved.
//

import UIKit

class MyTweetViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    
   
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier:"myTweet", for: indexPath)
        cell.textLabel?.text = "tweetler buraya gelicek"
        return cell
        
    }

}

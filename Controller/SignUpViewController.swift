//
//  SignUpViewController.swift
//  Twitter Clone
//
//  Created by Yıldırımhan Atçıoğlu on 16.01.2018.
//  Copyright © 2018 Yıldırımhan Atçıoğlu. All rights reserved.
//

import UIKit
import Firebase
class SignUpViewController: UIViewController {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passField: UITextField!
    @IBOutlet weak var passField2: UITextField!
   
    
    var ref = Database.database().reference()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    @IBAction func kaydolButton(_ sender: Any) {
        
        
        if(passField.text != passField2.text) // şifrelerin uyuşması kontrolü
        {
            print("şifreler birbiriyle uyuşmuyor!!")
        }
        else if ((emailField.text?.isEmpty)! && (passField.text?.isEmpty)! && (passField2.text?.isEmpty)!) // boşmu diye kontrol
            {
                
                print("alanlar boş lütfen doldurunuz")
                
                
            }
            else{
                
            print("şifreler uyuştu kaydınız yapılıyor.")
            Auth.auth().createUser(withEmail: emailField.text!, password: passField.text!, completion: { (user, error) in // firebase-signupfunc
                if (error != nil) // eğer hata verirse
                {
                    print(error!.localizedDescription)
                    print("kaydınız yapılamadı")
                }
                else
                {   // Kullanıcı kaydı başarıyla yapıldıysa
                    
                    self.ref.child("users").child(user!.uid).setValue(["email": self.emailField.text])
                    
                    print("kaydınız başarıyla tamamlanmıştır.")
                    self.emailField.text = ""
                    self.passField2.text = ""
                    self.passField.text = ""
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let SignInVC = storyboard.instantiateViewController(withIdentifier: "SignIn") as! SignInViewController
                    self.present(SignInVC, animated: true, completion: nil)
                }
                
            })
            
            
            
            
            
            }
            
        
        
       
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

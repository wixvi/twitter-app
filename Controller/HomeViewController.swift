//
//  HomeViewController.swift
//  Twitter Clone
//
//  Created by Yıldırımhan Atçıoğlu on 16.01.2018.
//  Copyright © 2018 Yıldırımhan Atçıoğlu. All rights reserved.
//

import UIKit
import Firebase
class HomeViewController: UIViewController {
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var fullname: UITextField!
    @IBOutlet weak var about: UITextField!
    
    var  ref = Database.database().reference()
    override func viewDidLoad() {
        super.viewDidLoad()

      
        
        
       
        
        
    }

   
    @IBAction func usernameUpdate(_ sender: Any)
    {
        //kullanıcının id sini almak
        let userId = Auth.auth().currentUser?.uid
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
       
        ref.child("usernames").child(self.username.text!).observeSingleEvent(of: .value, with: { (snapshot) in   //username alınmış mı kontrolü
            
            if(!snapshot.exists())
            {
                self.ref.child("users").child(userId!).child("username").setValue(self.username.text) //username childi üret
                self.ref.child("users").child(userId!).child("fullName").setValue(self.fullname.text) //same
                self.ref.child("users").child(userId!).child("about").setValue(self.about.text)       //same
                self.ref.child("usernames").child(self.username.text!).setValue(userId!)
                
                let timeLineVC = storyboard.instantiateViewController(withIdentifier: "TimeLineVC")
                self.present(timeLineVC, animated: true, completion: nil)
            }
            else {
                print("errorrrrrrrr")
            }
            
            // ...
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

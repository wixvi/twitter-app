//
//  SignInViewController.swift
//  Twitter Clone
//
//  Created by Yıldırımhan Atçıoğlu on 16.01.2018.
//  Copyright © 2018 Yıldırımhan Atçıoğlu. All rights reserved.
//

import UIKit
import Firebase
class SignInViewController: UIViewController {
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passField: UITextField!
    var  ref = Database.database().reference()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signIn(_ sender: Any) {
        
        if ((emailField.text?.isEmpty)! && (passField.text?.isEmpty)!)
        {
            print("Boş alan bırakmayın! ")
        }
        else
        {
            Auth.auth().signIn(withEmail: emailField.text!, password: passField.text!, completion: { (user, error) in
                if (error != nil)
                {
                    print(error!)
                }
                else
                {
                   
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    self.ref.child("users").child((user?.uid)!).child("username").observeSingleEvent(of: .value, with: { (snapshot) in
                        if(snapshot.exists())
                        {
                            let timeLineVC = storyboard.instantiateViewController(withIdentifier: "TimeLineVC")
                            self.present(timeLineVC, animated: true, completion: nil)
                        }
                        else
                        {
                            let homeVC = storyboard.instantiateViewController(withIdentifier: "HomeVC") as! HomeViewController
                            self.present(homeVC, animated: true, completion: nil)
                        }
                    })
                 
                  
                    
                }
            })
            
        }
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

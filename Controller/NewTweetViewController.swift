//
//  NewTweetViewController.swift
//  Twitter Clone
//
//  Created by Yıldırımhan Atçıoğlu on 18.01.2018.
//  Copyright © 2018 Yıldırımhan Atçıoğlu. All rights reserved.
//

import UIKit
import Firebase
class NewTweetViewController: UIViewController,UITextViewDelegate,UITextFieldDelegate {

    @IBOutlet weak var textField: UITextView!
    var ref = Database.database().reference()
    var user = Auth.auth().currentUser
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.text = "Tweetle!"
        textField.textColor = UIColor.lightGray
        
        textField.delegate = self
        // Do any additional setup after loading the view.
    }
    @IBAction func cancelButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if (textField.textColor == UIColor.lightGray)
        {
            textField.text = ""
            textField.textColor = UIColor.black
        }
    }
    @IBAction func tweetButton(_ sender: Any) {
        if(textField.text.count>0)
        {
            let uniqueID = ref.child("tweets").childByAutoId().key
            
            let date = String(Int(NSDate().timeIntervalSince1970))
            print(date)
            ref.child("tweets").child(user!.uid).child(uniqueID).child("tweet").setValue(textField.text)
            ref.child("tweets").child(user!.uid).child(uniqueID).child("date").setValue(date)
            
            dismiss(animated: true, completion: nil)
            
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

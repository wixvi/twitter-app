//
//  TimeLineViewController.swift
//  Twitter Clone
//
//  Created by Yıldırımhan Atçıoğlu on 17.01.2018.
//  Copyright © 2018 Yıldırımhan Atçıoğlu. All rights reserved.
//

import UIKit
import Firebase
class TimeLineViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
   
    
    
    
    
    var ref = Database.database().reference()
    var user = Auth.auth().currentUser

  //  @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var tweetsTableView: UITableView!
   
   
    
    var tweets = [String]()
    var dates = [String]()
    var userInfo = [String]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ref.child("users").child(user!.uid).observeSingleEvent(of: .value, with: { (snapshot) in
            let userValue = snapshot.value as? NSDictionary
            let name = userValue?["fullName"]
            let mentionname = userValue?["username"]
            
            self.userInfo.append(name as! String)
            self.userInfo.append(mentionname as! String)
            
            self.ref.child("tweets").child(self.user!.uid).observe(.childAdded, with: { (snapshot) in
                
             
                let value = snapshot.value as? NSDictionary
                let tweet = value?["tweet"]
                let date = value?["date"]
                
                self.tweets.append(tweet as! String)
                self.dates.append(date as! String)
                print(self.tweets)
                print(self.dates)
                
                self.tweetsTableView.insertRows(at: [IndexPath(row:self.tweets.count-1,section:0)], with: UITableViewRowAnimation.automatic)
                
                
                
               // self.loading.stopAnimating()
            })
            
            
            
            
            
            
        })
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tweets.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tweetCell", for: indexPath) as! SingleTweetWableViewCell
        cell.tweet.isEditable = false
        
        let imageView = cell.profilPic!
        imageView.layer.cornerRadius = 5.0
        imageView.layer.masksToBounds = true
        print(userInfo)
        
        cell.tweet.text = tweets[indexPath.row]
      //  cell.name.text = userInfo[0]
      //  cell.mentionName.text = userInfo[1]
        
        cell.confic(profilPic: "egg", name: self.userInfo[0], mentionName: self.userInfo[1])
        
        
        return cell
    }
    
    
    

   

}
